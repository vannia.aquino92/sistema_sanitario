package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the consultapaciente database table.
 * 
 */
@Entity
@NamedQuery(name="ConsultaPaciente.findAll", query="SELECT c FROM ConsultaPaciente c")
public class ConsultaPaciente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CONSULTAPACIENTE_IDCONSULTA_GENERATOR", sequenceName="CONSULTAPACIENTE_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CONSULTAPACIENTE_IDCONSULTA_GENERATOR")
	private Integer idconsulta;

	private String descripcion;

	private String tratamiento;

	//bi-directional many-to-one association to MedicoServicio
	@ManyToOne
	@JoinColumn(name="idmedserv")
	private MedicoServicio medicoservicio;

	//bi-directional many-to-one association to Paciente
	@ManyToOne
	@JoinColumn(name="cedula")
	private Paciente paciente;

	public ConsultaPaciente() {
	}

	public Integer getIdconsulta() {
		return this.idconsulta;
	}

	public void setIdconsulta(Integer idconsulta) {
		this.idconsulta = idconsulta;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTratamiento() {
		return this.tratamiento;
	}

	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}

	public MedicoServicio getMedicoservicio() {
		return this.medicoservicio;
	}

	public void setMedicoservicio(MedicoServicio medicoservicio) {
		this.medicoservicio = medicoservicio;
	}

	public Paciente getPaciente() {
		return this.paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

}