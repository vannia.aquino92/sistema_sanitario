package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the serviciohospit database table.
 * 
 */
@Entity
@NamedQuery(name="ServicioHospit.findAll", query="SELECT s FROM ServicioHospit s")
public class ServicioHospit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SERVICIOHOSPIT_IDSERVHOSP_GENERATOR", sequenceName="SERVICIOHOSPIT_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SERVICIOHOSPIT_IDSERVHOSP_GENERATOR")
	private Integer idservhosp;

	private Integer cantcamas;

	//bi-directional many-to-one association to Internacion
	@OneToMany(mappedBy="serviciohospit")
	private Set<Internacion> internacions;

	//bi-directional many-to-one association to MedicoServicio
	@OneToMany(mappedBy="serviciohospit")
	private Set<MedicoServicio> medicoservicios;

	//bi-directional many-to-one association to Hospital
	@ManyToOne
	@JoinColumn(name="codhospital")
	private Hospital hospitale;

	//bi-directional many-to-one association to Servicio
	@ManyToOne
	@JoinColumn(name="idservicio")
	private Servicio servicio;

	public ServicioHospit() {
	}

	public Integer getIdservhosp() {
		return this.idservhosp;
	}

	public void setIdservhosp(Integer idservhosp) {
		this.idservhosp = idservhosp;
	}

	public Integer getCantcamas() {
		return this.cantcamas;
	}

	public void setCantcamas(Integer cantcamas) {
		this.cantcamas = cantcamas;
	}

	public Set<Internacion> getInternacions() {
		return this.internacions;
	}

	public void setInternacions(Set<Internacion> internacions) {
		this.internacions = internacions;
	}

	public Internacion addInternacion(Internacion internacion) {
		getInternacions().add(internacion);
		internacion.setServiciohospit(this);

		return internacion;
	}

	public Internacion removeInternacion(Internacion internacion) {
		getInternacions().remove(internacion);
		internacion.setServiciohospit(null);

		return internacion;
	}

	public Set<MedicoServicio> getMedicoservicios() {
		return this.medicoservicios;
	}

	public void setMedicoservicios(Set<MedicoServicio> medicoservicios) {
		this.medicoservicios = medicoservicios;
	}

	public MedicoServicio addMedicoservicio(MedicoServicio medicoservicio) {
		getMedicoservicios().add(medicoservicio);
		medicoservicio.setServiciohospit(this);

		return medicoservicio;
	}

	public MedicoServicio removeMedicoservicio(MedicoServicio medicoservicio) {
		getMedicoservicios().remove(medicoservicio);
		medicoservicio.setServiciohospit(null);

		return medicoservicio;
	}

	public Hospital getHospitale() {
		return this.hospitale;
	}

	public void setHospitale(Hospital hospitale) {
		this.hospitale = hospitale;
	}

	public Servicio getServicio() {
		return this.servicio;
	}

	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

}