package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the medicos database table.
 * 
 */
@Entity
@Table(name="medicos")
@NamedQuery(name="Medico.findAll", query="SELECT m FROM Medico m")
public class Medico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MEDICOS_DNI_GENERATOR", sequenceName="MEDICO_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MEDICOS_DNI_GENERATOR")
	private Integer dni;

	private String apellido;

	@Temporal(TemporalType.DATE)
	private Date fechanacimiento;

	private String nombre;

	//bi-directional many-to-one association to Hospital
	@OneToMany(mappedBy="medico")
	private Set<Hospital> hospitales;

	//bi-directional many-to-one association to Hospital
	@ManyToOne
	@JoinColumn(name="hospitaladscrito")
	private Hospital hospitale;

	//bi-directional many-to-one association to MedicoServicio
	@OneToMany(mappedBy="medico")
	private Set<MedicoServicio> medicoservicios;

	public Medico() {
	}

	public Integer getDni() {
		return this.dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Date getFechanacimiento() {
		return this.fechanacimiento;
	}

	public void setFechanacimiento(Date fechanacimiento) {
		this.fechanacimiento = fechanacimiento;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set<Hospital> getHospitales() {
		return this.hospitales;
	}

	public void setHospitales(Set<Hospital> hospitales) {
		this.hospitales = hospitales;
	}

	public Hospital addHospitale(Hospital hospitale) {
		getHospitales().add(hospitale);
		hospitale.setMedico(this);

		return hospitale;
	}

	public Hospital removeHospitale(Hospital hospitale) {
		getHospitales().remove(hospitale);
		hospitale.setMedico(null);

		return hospitale;
	}

	public Hospital getHospitale() {
		return this.hospitale;
	}

	public void setHospitale(Hospital hospitale) {
		this.hospitale = hospitale;
	}

	public Set<MedicoServicio> getMedicoservicios() {
		return this.medicoservicios;
	}

	public void setMedicoservicios(Set<MedicoServicio> medicoservicios) {
		this.medicoservicios = medicoservicios;
	}

	public MedicoServicio addMedicoservicio(MedicoServicio medicoservicio) {
		getMedicoservicios().add(medicoservicio);
		medicoservicio.setMedico(this);

		return medicoservicio;
	}

	public MedicoServicio removeMedicoservicio(MedicoServicio medicoservicio) {
		getMedicoservicios().remove(medicoservicio);
		medicoservicio.setMedico(null);

		return medicoservicio;
	}

}