package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the hospitales database table.
 * 
 */
@Entity
@Table(name="hospitales")
@NamedQuery(name="Hospital.findAll", query="SELECT h FROM Hospital h")
public class Hospital implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="HOSPITALES_CODHOSPITAL_GENERATOR", sequenceName="HOSPITAL_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOSPITALES_CODHOSPITAL_GENERATOR")
	private Integer codhospital;

	private String nombre;

	//bi-directional many-to-one association to Ciudad
	@ManyToOne
	@JoinColumn(name="ciudad")
	private Ciudad ciudade;

	//bi-directional many-to-one association to Medico
	@ManyToOne
	@JoinColumn(name="director")
	private Medico medico;

	//bi-directional many-to-one association to Medico
	@OneToMany(mappedBy="hospitale")
	private Set<Medico> medicos;

	//bi-directional many-to-one association to ServicioHospit
	@OneToMany(mappedBy="hospitale")
	private Set<ServicioHospit> serviciohospits;

	//bi-directional many-to-one association to TelefonosHospital
	@OneToMany(mappedBy="hospitale")
	private Set<TelefonosHospital> telefonoshospitals;

	public Hospital() {
	}

	public Integer getCodhospital() {
		return this.codhospital;
	}

	public void setCodhospital(Integer codhospital) {
		this.codhospital = codhospital;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Ciudad getCiudade() {
		return this.ciudade;
	}

	public void setCiudade(Ciudad ciudade) {
		this.ciudade = ciudade;
	}

	public Medico getMedico() {
		return this.medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public Set<Medico> getMedicos() {
		return this.medicos;
	}

	public void setMedicos(Set<Medico> medicos) {
		this.medicos = medicos;
	}

	public Medico addMedico(Medico medico) {
		getMedicos().add(medico);
		medico.setHospitale(this);

		return medico;
	}

	public Medico removeMedico(Medico medico) {
		getMedicos().remove(medico);
		medico.setHospitale(null);

		return medico;
	}

	public Set<ServicioHospit> getServiciohospits() {
		return this.serviciohospits;
	}

	public void setServiciohospits(Set<ServicioHospit> serviciohospits) {
		this.serviciohospits = serviciohospits;
	}

	public ServicioHospit addServiciohospit(ServicioHospit serviciohospit) {
		getServiciohospits().add(serviciohospit);
		serviciohospit.setHospitale(this);

		return serviciohospit;
	}

	public ServicioHospit removeServiciohospit(ServicioHospit serviciohospit) {
		getServiciohospits().remove(serviciohospit);
		serviciohospit.setHospitale(null);

		return serviciohospit;
	}

	public Set<TelefonosHospital> getTelefonoshospitals() {
		return this.telefonoshospitals;
	}

	public void setTelefonoshospitals(Set<TelefonosHospital> telefonoshospitals) {
		this.telefonoshospitals = telefonoshospitals;
	}

	public TelefonosHospital addTelefonoshospital(TelefonosHospital telefonoshospital) {
		getTelefonoshospitals().add(telefonoshospital);
		telefonoshospital.setHospitale(this);

		return telefonoshospital;
	}

	public TelefonosHospital removeTelefonoshospital(TelefonosHospital telefonoshospital) {
		getTelefonoshospitals().remove(telefonoshospital);
		telefonoshospital.setHospitale(null);

		return telefonoshospital;
	}

}