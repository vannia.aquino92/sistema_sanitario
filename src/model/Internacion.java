package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the internacion database table.
 * 
 */
@Entity
@NamedQuery(name="Internacion.findAll", query="SELECT i FROM Internacion i")
public class Internacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="INTERNACION_IDINTERNACION_GENERATOR", sequenceName="INTERNACION_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INTERNACION_IDINTERNACION_GENERATOR")
	private Integer idinternacion;

	@Temporal(TemporalType.DATE)
	private Date fechaalta;

	private String nrohabitacion;

	//bi-directional many-to-one association to Paciente
	@ManyToOne
	@JoinColumn(name="paciente")
	private Paciente pacienteBean;

	//bi-directional many-to-one association to ServicioHospit
	@ManyToOne
	@JoinColumn(name="idservhosp")
	private ServicioHospit serviciohospit;

	public Internacion() {
	}

	public Integer getIdinternacion() {
		return this.idinternacion;
	}

	public void setIdinternacion(Integer idinternacion) {
		this.idinternacion = idinternacion;
	}

	public Date getFechaalta() {
		return this.fechaalta;
	}

	public void setFechaalta(Date fechaalta) {
		this.fechaalta = fechaalta;
	}

	public String getNrohabitacion() {
		return this.nrohabitacion;
	}

	public void setNrohabitacion(String nrohabitacion) {
		this.nrohabitacion = nrohabitacion;
	}

	public Paciente getPacienteBean() {
		return this.pacienteBean;
	}

	public void setPacienteBean(Paciente pacienteBean) {
		this.pacienteBean = pacienteBean;
	}

	public ServicioHospit getServiciohospit() {
		return this.serviciohospit;
	}

	public void setServiciohospit(ServicioHospit serviciohospit) {
		this.serviciohospit = serviciohospit;
	}

}