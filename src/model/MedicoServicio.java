package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the medicoservicios database table.
 * 
 */
@Entity
@Table(name="medicoservicios")
@NamedQuery(name="MedicoServicio.findAll", query="SELECT m FROM MedicoServicio m")
public class MedicoServicio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MEDICOSERVICIOS_IDMEDSERV_GENERATOR", sequenceName="MEDICOSERVICIO_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MEDICOSERVICIOS_IDMEDSERV_GENERATOR")
	private Integer idmedserv;

	//bi-directional many-to-one association to ConsultaPaciente
	@OneToMany(mappedBy="medicoservicio")
	private Set<ConsultaPaciente> consultapacientes;

	//bi-directional many-to-one association to Medico
	@ManyToOne
	@JoinColumn(name="dni")
	private Medico medico;

	//bi-directional many-to-one association to ServicioHospit
	@ManyToOne
	@JoinColumn(name="idservhosp")
	private ServicioHospit serviciohospit;

	public MedicoServicio() {
	}

	public Integer getIdmedserv() {
		return this.idmedserv;
	}

	public void setIdmedserv(Integer idmedserv) {
		this.idmedserv = idmedserv;
	}

	public Set<ConsultaPaciente> getConsultapacientes() {
		return this.consultapacientes;
	}

	public void setConsultapacientes(Set<ConsultaPaciente> consultapacientes) {
		this.consultapacientes = consultapacientes;
	}

	public ConsultaPaciente addConsultapaciente(ConsultaPaciente consultapaciente) {
		getConsultapacientes().add(consultapaciente);
		consultapaciente.setMedicoservicio(this);

		return consultapaciente;
	}

	public ConsultaPaciente removeConsultapaciente(ConsultaPaciente consultapaciente) {
		getConsultapacientes().remove(consultapaciente);
		consultapaciente.setMedicoservicio(null);

		return consultapaciente;
	}

	public Medico getMedico() {
		return this.medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public ServicioHospit getServiciohospit() {
		return this.serviciohospit;
	}

	public void setServiciohospit(ServicioHospit serviciohospit) {
		this.serviciohospit = serviciohospit;
	}

}