package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the paciente database table.
 * 
 */
@Entity
@NamedQuery(name="Paciente.findAll", query="SELECT p FROM Paciente p")
public class Paciente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PACIENTE_CEDULA_GENERATOR", sequenceName="PACIENTE_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PACIENTE_CEDULA_GENERATOR")
	private Integer cedula;

	private String apellido;

	@Temporal(TemporalType.DATE)
	private Date fechanacimiento;

	private String nombre;

	//bi-directional many-to-one association to ConsultaPaciente
	@OneToMany(mappedBy="paciente")
	private Set<ConsultaPaciente> consultapacientes;

	//bi-directional many-to-one association to Internacion
	@OneToMany(mappedBy="pacienteBean")
	private Set<Internacion> internacions;

	public Paciente() {
	}

	public Integer getCedula() {
		return this.cedula;
	}

	public void setCedula(Integer cedula) {
		this.cedula = cedula;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Date getFechanacimiento() {
		return this.fechanacimiento;
	}

	public void setFechanacimiento(Date fechanacimiento) {
		this.fechanacimiento = fechanacimiento;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set<ConsultaPaciente> getConsultapacientes() {
		return this.consultapacientes;
	}

	public void setConsultapacientes(Set<ConsultaPaciente> consultapacientes) {
		this.consultapacientes = consultapacientes;
	}

	public ConsultaPaciente addConsultapaciente(ConsultaPaciente consultapaciente) {
		getConsultapacientes().add(consultapaciente);
		consultapaciente.setPaciente(this);

		return consultapaciente;
	}

	public ConsultaPaciente removeConsultapaciente(ConsultaPaciente consultapaciente) {
		getConsultapacientes().remove(consultapaciente);
		consultapaciente.setPaciente(null);

		return consultapaciente;
	}

	public Set<Internacion> getInternacions() {
		return this.internacions;
	}

	public void setInternacions(Set<Internacion> internacions) {
		this.internacions = internacions;
	}

	public Internacion addInternacion(Internacion internacion) {
		getInternacions().add(internacion);
		internacion.setPacienteBean(this);

		return internacion;
	}

	public Internacion removeInternacion(Internacion internacion) {
		getInternacions().remove(internacion);
		internacion.setPacienteBean(null);

		return internacion;
	}

}