package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the telefonoshospital database table.
 * 
 */
@Entity
@NamedQuery(name="TelefonosHospital.findAll", query="SELECT t FROM TelefonosHospital t")
public class TelefonosHospital implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TELEFONOSHOSPITAL_CODTELEFONOHOSPITAL_GENERATOR", sequenceName="TELEFONOHOSPITAL_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TELEFONOSHOSPITAL_CODTELEFONOHOSPITAL_GENERATOR")
	private Integer codtelefonohospital;

	private String numero;

	//bi-directional many-to-one association to Hospital
	@ManyToOne
	@JoinColumn(name="codhospital")
	private Hospital hospitale;

	public TelefonosHospital() {
	}

	public Integer getCodtelefonohospital() {
		return this.codtelefonohospital;
	}

	public void setCodtelefonohospital(Integer codtelefonohospital) {
		this.codtelefonohospital = codtelefonohospital;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Hospital getHospitale() {
		return this.hospitale;
	}

	public void setHospitale(Hospital hospitale) {
		this.hospitale = hospitale;
	}

}