package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the ciudades database table.
 * 
 */
@Entity
@Table(name="ciudades")
@NamedQuery(name="Ciudad.findAll", query="SELECT c FROM Ciudad c")
public class Ciudad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CIUDADES_CODCIUDAD_GENERATOR", sequenceName="CIUDAD_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CIUDADES_CODCIUDAD_GENERATOR")
	private Integer codciudad;

	private String descripcion;

	//bi-directional many-to-one association to Hospital
	@OneToMany(mappedBy="ciudade")
	private Set<Hospital> hospitales;

	public Ciudad() {
	}

	public Integer getCodciudad() {
		return this.codciudad;
	}

	public void setCodciudad(Integer codciudad) {
		this.codciudad = codciudad;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Set<Hospital> getHospitales() {
		return this.hospitales;
	}

	public void setHospitales(Set<Hospital> hospitales) {
		this.hospitales = hospitales;
	}

	public Hospital addHospitale(Hospital hospitale) {
		getHospitales().add(hospitale);
		hospitale.setCiudade(this);

		return hospitale;
	}

	public Hospital removeHospitale(Hospital hospitale) {
		getHospitales().remove(hospitale);
		hospitale.setCiudade(null);

		return hospitale;
	}

}