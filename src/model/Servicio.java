package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the servicios database table.
 * 
 */
@Entity
@Table(name="servicios")
@NamedQuery(name="Servicio.findAll", query="SELECT s FROM Servicio s")
public class Servicio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SERVICIOS_IDSERVICIO_GENERATOR", sequenceName="SERVICIO_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SERVICIOS_IDSERVICIO_GENERATOR")
	private String idservicio;

	private String comentario;

	private String nombre;

	//bi-directional many-to-one association to ServicioHospit
	@OneToMany(mappedBy="servicio")
	private Set<ServicioHospit> serviciohospits;

	public Servicio() {
	}

	public String getIdservicio() {
		return this.idservicio;
	}

	public void setIdservicio(String idservicio) {
		this.idservicio = idservicio;
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set<ServicioHospit> getServiciohospits() {
		return this.serviciohospits;
	}

	public void setServiciohospits(Set<ServicioHospit> serviciohospits) {
		this.serviciohospits = serviciohospits;
	}

	public ServicioHospit addServiciohospit(ServicioHospit serviciohospit) {
		getServiciohospits().add(serviciohospit);
		serviciohospit.setServicio(this);

		return serviciohospit;
	}

	public ServicioHospit removeServiciohospit(ServicioHospit serviciohospit) {
		getServiciohospits().remove(serviciohospit);
		serviciohospit.setServicio(null);

		return serviciohospit;
	}

}